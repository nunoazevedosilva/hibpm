# HIBPM
The HiBPM is a high-performance cross-platform numerical module to solve the Generalized Nonlinear Schrodinger Poisson equation system(GNLSP) using a Split-Step Fourier Method. The code is written in C++ and uses the Arrayfire library as a major component.

## Previous notes
The code was mainly tested with Visual Studio 2017 or 2019 and therefore Windows is recommended.  Yet, we confirmed it can run in other operating systems, but remains largely untested on other development environments.

## External dependencies
The code requires the installation of the Arrayfire library that you can download at https://arrayfire.com/. To run each backend, you need to install the required runtime(OpenCL or CUDA). Everything else you will need to get up and running is included in the repository.
Furthermore, as it uses the ***filesystem*** of the standard library, it requires a C++17 standard or higher.
The easiest way to get started is to clone the repository to a local destination using git, 

`git clone https://nunoazevedosilva@bitbucket.org/nunoazevedosilva/hibpm.git`

## Code Structure
The code is written in a modular structure that can be easily adapted. The global organization is depicted in the following picture:

![ima](https://bitbucket.org/nunoazevedosilva/hibpm/raw/f1e6c24e2287ee24a6a0db2665d3936f3288bb08/images/code_structure.jpg){width=10%}

## Run your first simulation

We recommend to build the project using CMake, for which we provide the ***CMakeLists.txt*** file in the ***src*** folder. Both 1d and 2d examples are defined in the ***sample_GNLSP_problem.h***  file, which is properly commented along so you can follow. The code in ***Source.cpp*** exploits the Unified backend of Arrayfire and asks for a choice the running shell, as well as for the device you want to use for computing purposes.

### Building in Windows

The easiest way to build the project correctly is to use CMake, that you can download at https://cmake.org/download/ . After installing it, 

### Building in Linux

Again we recommend using CMake. With CMake installed, building and running the solution is relatively easy:

## Handling data
The usual output of the simulation is a large amount of data in a raw Arrayfire format. In order to allow further processing of the data and to ease the interpretation of the results, we developed a series of Python scripts. In the post-processing folder the ***af_loader.py*** script provides a loader for the data, which converts from the Arrayfire format into a numpy array. Then, depending on the dimensionality of the problem and the type of treatment of the data needed, one can rely on the more common matplotlib plot functionalities or use the converter we developed to save the data into a vtk format which can be loaded to be used in Paraview. In the same folder we also provide an example script for visualizing 1d and 2d simulations.

# License
The code is freely distributed for personal use and can be adapted as wish.  We just ask that you cite the repository as

**Azevedo Silva, Nuno and Ferreira, Tiago, HiBPM(2020), GitHub repository, https://bitbucket.org/nunoazevedosilva/hibpm**

and include an authorship acknowledgment in any codes that use its structure.

# Publication with further information

# Some publications using this simulation tool
Exploring quantum-like turbulence with a two-component paraxial fluid of light
NA Silva, TD Ferreira, A Guerreiro - Results in Optics, 2020

Dissipative solitons in an atomic medium assisted by an incoherent pumping field
NA Silva, AL Almeida, TD Ferreira, A Guerreiro - Journal of Physics B: Atomic, Molecular and Optical Physics 53 (6), 065401

Using numerical methods from nonlocal optics to simulate the dynamics of N-body systems in alternative theories of gravity
TD Ferreira, NA Silva, O Bertolami, C Gomes - Physical Review E, 2020

Superfluidity of light in nematic liquid crystals
TD Ferreira, NA Silva, A Guerreiro - Physical Review A 98 (2), 023825

Persistent currents of superfluidic light in a four-level coherent atomic medium
NA Silva, JT Mendonca, A Guerreiro - JOSA B, 2017

# Citation
Azevedo Silva, Nuno and Ferreira, Tiago, HiBPM(2020), GitHub repository, https://bitbucket.org/nunoazevedosilva/hibpm

