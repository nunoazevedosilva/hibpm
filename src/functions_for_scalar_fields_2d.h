#pragma once

#include "definitions.h"
#include "mesh.h"



/**

functions_for_scalar_fields_2d.h
Purpose: Various functions definitions for generating different scalar field profiles

@author Nuno Azevedo Silva
@version 1.0

*/


/*
gaussian_2d creates a vector with a 2d gaussian state

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param w is the width
@param x0 is the center in x-direction
@param y0 is the center in y-direction
@return array
*/
af::array gaussian_2d(mesh pmesh, cPrecision A, cPrecision w, Precision x0, Precision y0)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::exp(-((pmesh.x() - x0)*(pmesh.x() - x0) + (pmesh.y() - y0)*(pmesh.y() - y0)) / (w*w));

	return new_vector;
}

af::array noise_2d(mesh pmesh, cPrecision A)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::randn(pmesh.Nx, pmesh.Ny, pmesh.Nt, 1);

	return new_vector;
}


af::array quantum_noise(mesh pmesh, int M, Precision V)
{
	srand(1);
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	for (int mi=0;mi<M;mi++)
	{
		for (int mj = 0; mj < M; mj++)
		{
			Precision alpha = rand() % 1000 * 0.001;
			alpha = alpha * sqrt(2.0) - 1.0 / sqrt(2);
			Precision beta = sqrt(0.5 - alpha * alpha);
			new_vector += 1/sqrt(V);
		}
	}

	return new_vector;
}


/*
gaussian_2d creates a vector with a 2d gaussian state

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param w is the width
@param x0 is the center in x-direction
@param y0 is the center in y-direction
@param vx is the velocity in x - direction
@param vy is the velocity in y - direction
@return array
*/
af::array gaussian_2d(mesh pmesh, cPrecision A, cPrecision w, Precision x0, Precision y0, Precision vx, Precision vy)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::exp(-((pmesh.x() - x0)*(pmesh.x() - x0)+ (pmesh.y() - y0)*(pmesh.y() - y0)) / (w*w))*af::exp(cPrecision(0,1)*(vx*(pmesh.x()-x0)+ vy*(pmesh.y()-y0)));

	return new_vector;
}

/*
gaussian_2d creates a vector with a 2d gaussian state

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param w is the width
@param x0 is the center in x-direction
@param y0 is the center in y-direction
@return array
*/
af::array bessel_2d(mesh pmesh, cPrecision A, Precision w, Precision x0, Precision y0)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	for (int i = 0; i < pmesh.Nx; i++)
	{
		for (int j = 0; j < pmesh.Ny; j++)
		{

			Precision x1 = i*pmesh.dx;
			Precision y1 = j*pmesh.dy;
			Precision r = sqrt((x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0));
			new_vector(i, j, 0, 0) += A * jn(0, r / w);

		}
	
	}

	return new_vector;
}


/*
perfect vortex 
@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param w is the width
@param x0 is the center in x-direction
@param y0 is the center in y-direction
@return array
*/
af::array perfect_vortex_2d(mesh pmesh, cPrecision A, cPrecision dr, Precision x0, Precision y0, Precision r0, Precision m)
{

	af::array r = af::sqrt((pmesh.x() - x0)*(pmesh.x() - x0) + (pmesh.y() - y0)*(pmesh.y() - y0));
	af::array theta = af::atan2((pmesh.y() - y0),(pmesh.x() - x0));
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::exp(-(r - r0)*(r - r0) / (dr*dr))*af::exp(cPrecision(0, 1.0)*m*theta);

	return new_vector;
}

/*
perfect vortex
@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param w is the width
@param x0 is the center in x-direction
@param y0 is the center in y-direction
@return array
*/
af::array perfect_vortex_2d_2(mesh pmesh, cPrecision A, cPrecision dr, Precision x0, Precision y0, Precision r0, Precision m)
{

	af::array r = af::sqrt((pmesh.x() - x0)*(pmesh.x() - x0) + (pmesh.y() - y0)*(pmesh.y() - y0));
	af::array theta = af::atan2((pmesh.y() - y0), (pmesh.x() - x0));
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::exp(-(r - r0)*(r - r0) / (dr*dr));

	return new_vector;
}

/*
perfect vortex
@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param w is the width
@param x0 is the center in x-direction
@param y0 is the center in y-direction
@return array
*/
af::array plate(mesh pmesh, cPrecision A, cPrecision a, Precision x0, Precision y0, Precision xs, Precision ys)
{
		af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::exp((ys*(pmesh.y()-y0)*ys*(pmesh.y() - y0)+ xs*(pmesh.x() - x0)*xs*(pmesh.x() - x0))/(-2*a*a));

	return new_vector;
}



/*
plane_wave_2d creates an array with a 2d plane wave

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param kx is the k in x direction
@param ky is the k in y direction
@return array
*/
af::array planewave_2d(mesh pmesh, cPrecision A,  Precision kx, Precision ky)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A* af::exp(cPrecision(0, 1)*(kx*pmesh.x()));//+ky*pmesh.y()));

	return new_vector;
}

/*
rectangle creates an array with a rectangle

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param kx is the k in x direction
@param ky is the k in y direction
@return array
*/
af::array rectangle(mesh pmesh, cPrecision A, Precision x0, Precision y0, Precision Lx, Precision Ly)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * (pmesh.x()>(x0-Lx/2.))*(pmesh.x() < (x0 + Lx / 2.))*(pmesh.y() > (y0 - Ly / 2.))*(pmesh.y() < (y0 + Ly / 2.));
	return new_vector;
}

/*
rectangle creates an array with a rectangle

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param kx is the k in x direction
@param ky is the k in y direction
@return array
*/
af::array gravity(mesh pmesh, cPrecision A)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A*pmesh.y();
	return new_vector;
}

af::array dirac(mesh pmesh, cPrecision x0)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector(pmesh.Nx/2,pmesh.Ny/2,0,0)=1.;

	return new_vector;
}




/*
supergaussian_2d creates a vector with a 1d supergaussian state

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param a is the width parameter
@param m is the power
@param zxs is the k, velocity in x direction
@param x0 is the center x
@param phase is the initial phase for the state
@param xs is a scale in x direction
@return array
*/
af::array supergaussian_2d(mesh pmesh, cPrecision A = 1, cPrecision a = 1, Precision m = 1, Precision zxs = 0, Precision zys = 0, Precision x0 = 0, Precision y0 = 0, Precision phase = 0, Precision xs = 1, Precision ys = 1)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);
	new_vector += A*af::exp(af::pow(((pmesh.x()*xs - x0)*(pmesh.x()*xs - x0)+ (pmesh.y()*ys - y0)*(pmesh.y()*ys - y0)) / (a*a), m) / cPrecision(-2.) + cPrecision(0, 1)*zxs*(pmesh.x() - x0) + cPrecision(0, 1)*zys*(pmesh.y() - y0) + cPrecision(0, 1)*phase);
	return new_vector;
};

/*
squarevortex creates a vector with a square vortex
@return array
*/
af::array squarevortex(mesh pmesh, cPrecision A = 1, cPrecision r0 = 1, cPrecision w = 1, Precision l = 1, Precision x0 = 0, Precision y0 = 0)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);
	af::array r = af::sqrt((pmesh.x()-x0)*(pmesh.x() - x0) + (pmesh.y()-y0)*(pmesh.y()-y0));
	af::array theta = af::atan2(pmesh.y() - y0, pmesh.x() - x0);
	new_vector += A*(af::abs(r-r0)<w)*af::exp(cPrecision(0,1)*theta*l);
	return new_vector;
};