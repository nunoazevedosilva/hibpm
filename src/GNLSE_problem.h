#pragma once

/************************************************
 *  HiBPM: a numerical module to solve the Generalized
 Nonlinear Schrödinger Poisson equation system(GNLSP).
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/

 /*
GNLSE_problem.h:
An instance of this class encapsulates all the objects generated
by above classes to create a problem corresponding to a GNLSE. Contains
a method solve which performs the numerical integration of the system with the
pseudospectral algorithm we chosen as many times as required.
 */

#include "core.h"
#include "definitions.h"
#include "mesh.h"
#include "nonlinear_Terms.h"
#include "Misc.h"
#include "gnlse_field.h"


class GNLSE_problem
{

public:

	gnlse_field& envelope;			//the scalar field object
	mesh& pmesh;					//our mesh for the problem
	nonlinear_terms& nonlin;		//the nonlinearities object
	cPrecision l_factor;			//number factor in laplacian
	std::string nl_integrator;		//type of integrator (Euler for now but can be different)

	af::array k2;
	af::array exp_lin;

	GNLSE_problem(mesh& pmesh, gnlse_field& envelope, nonlinear_terms& nonlin, cPrecision l_factor, std::string nl_integrator = "Euler");

	void solve(int stride, int total_strides, std::string saveDir);

	void solve(int stride, int total_strides, af::Window& win);

	void solve(int stride, int total_strides, std::string saveDir, af::Window& win);

	void benchmark_solve(int stride, int total_strides, std::string saveDir);

	void linear_step(Precision dz);

	void nonlinear_step_vector_euler(Precision dz);

	void nonlinear_step_number_euler(Precision dz);

	void push_vectors_and_numbers(int stride);

	void push_vectors(int stride);

	void push_numbers(int stride);

	void plot(af::Window& win);

};

/*
GNLSE_problem class constructor

@param pmesh is the mesh where we construct our scalar field vector
@param envelope is the profile, should be a previously created scalar field
@param l_factor is the number factor in the laplacian
@param nl_integrator is the type of integrator for the nonlinear step, "Euler"
@return Null
*/
GNLSE_problem::GNLSE_problem(mesh& pmesh, gnlse_field& envelope, nonlinear_terms& nonlin, cPrecision l_factor, std::string nl_integrator) : pmesh(pmesh),
															envelope(envelope), nonlin(nonlin), l_factor(l_factor), nl_integrator(nl_integrator) 
{
	k2 = k2_nd(pmesh.dx, pmesh.dy, pmesh.dt, envelope.profile.dims(), Precision3);
	k2.eval();

	exp_lin = af::exp(-pmesh.dz * k2 * l_factor * cPrecision(0.0, 1.0)) / (pmesh.Nx*pmesh.Ny*pmesh.Nt);
	exp_lin.eval();
}


//method for preforming the linear step of dt of the Split-step fourier method
void GNLSE_problem::linear_step(Precision dz)
{
	//make k2 at each iteration - it saves memory but can become heavy -  think in an alternative having k2 outside - 
	//it will need a profile-sized vector
	//reducing the max memory to half...

	envelope.profile = af::dft(envelope.profile, 1.0, envelope.profile.dims());
	//multiply by exp (dt * k2 *...) stuff - remember of the half-step so we need to divide dt by 2
	envelope.profile *= af::exp(- dz * k2 * l_factor * cPrecision(0.0, 1.0))/(pmesh.Nx*pmesh.Ny*pmesh.Nt);
	//envelope.profile *= exp_lin;
	//recover in direct space
	envelope.profile = af::idft(envelope.profile, 1.0, envelope.profile.dims());
}

//method for performing a nonlinear step of dt - most simple euler implementation - for vector type nonlinearities
void GNLSE_problem::nonlinear_step_vector_euler(Precision dz)
{
	//number of vector nonlinearities
	int size_vecs = nonlin.vectors.typenl.size();

	for (int i = 0; i<size_vecs; i++)
	{
		//perform nonlinear step for each nonlinearity
		envelope.profile *= af::exp(cPrecision(0,1)*dz*nonlin.vectors.nonlinearities[i]
			* af::pow(af::abs(envelope.profile), nonlin.vectors.nonlinear_power[i]));
	}
	envelope.profile.eval();

}

//method for performing a nonlinear step of dt - most simple euler implementation - for number type nonlinearities
void GNLSE_problem::nonlinear_step_number_euler(Precision dz)
{
	//number of vector nonlinearities 
	int size_vecs = nonlin.numbers.typenl.size();

	for (int i = 0; i<size_vecs; i++)
	//for (int i = 0; i<1; i++)
	{
		//perform nonlinear step for each nonlinearity
		envelope.profile =envelope.profile*af::exp(cPrecision(0, 1)*dz*nonlin.numbers.nonlinearities[i]
			* af::pow(af::abs(envelope.profile), nonlin.numbers.nonlinear_power[i]));
	}

}

//method for a time integration stride for the usual Nonlinear Schrodinger equation - both vectors and numbers
void GNLSE_problem::push_vectors_and_numbers(int stride)
{
	Precision dz = pmesh.dz;

		//first a half linear step
		linear_step(dz/2);
		envelope.profile.eval();
		for (int step = 0; step < stride-1; step++)
		{
			//now perform each nonlinear step
			nonlinear_step_vector_euler(dz);//nonlinear step with vector nonlinearities
			nonlinear_step_number_euler(dz);//nonlinear step with number nonnlinearities
			//followed by a full dt linear step
			linear_step(dz);
			envelope.profile.eval();
		}
		
		//last nonlinear step for the stride
		nonlinear_step_vector_euler(dz);//nonlinear step with vector nonlinearities
		nonlinear_step_number_euler(dz);//nonlinear step with number nonnlinearities

		//a half linear step ends the stride
		linear_step(dz/2);
		envelope.profile.eval();
	

	//update step number and time coordinate
	pmesh.current_step_number += stride;
	pmesh.current_z += pmesh.dz * stride;
}

//method for a time integration step for the usual Nonlinear Schrodinger equation - only with vector nonlinearities
void GNLSE_problem::push_vectors(int stride)
{
	if (nl_integrator == "Euler")
	{
		//first a half linear step
		linear_step(pmesh.dz / 2);

		for (int step = 0; step < stride - 1; step++)
		{
			//now perform each nonlinear step
			nonlinear_step_vector_euler(pmesh.dz);//nonlinear step with vector nonlinearities
												  //followed by a full dt linear step
			linear_step(pmesh.dz);
		}

		//last nonlinear step for the stride
		nonlinear_step_vector_euler(pmesh.dz);//nonlinear step with vector nonlinearities
											  //a half linear step ends the stride
		linear_step(pmesh.dz / 2);
	}

	//update step number and time coordinate
	pmesh.current_step_number += stride;
	pmesh.current_z += pmesh.dz * stride;
}

//method for a time integration step for the usual Nonlinear Schrodinger equation - only numbers
void GNLSE_problem::push_numbers(int stride)
{
	if (nl_integrator == "Euler")
	{
		//first a half linear step
		linear_step(pmesh.dz / 2);

		for (int step = 0; step < stride - 1; step++)
		{
			//now perform each nonlinear step
			nonlinear_step_number_euler(pmesh.dz);//nonlinear step with number nonnlinearities
												  //followed by a full dt linear step
			linear_step(pmesh.dz);
		}

		//last nonlinear step for the stride
		nonlinear_step_number_euler(pmesh.dz);//nonlinear step with number nonnlinearities
											  //a half linear step ends the stride
		linear_step(pmesh.dz / 2);
	}

	//update step number and time coordinate
	pmesh.current_step_number += stride;
	pmesh.current_z += pmesh.dz * stride;
}

void GNLSE_problem::plot(af::Window& win)
{
	//1d version
	af::array X = pmesh.x()(af::span, 0).as(f32);
	af::array Y = af::abs(envelope.profile(af::span, 0)).as(f32);
	win.plot(X, Y);

	//2d version

	//3d version


}

/*
method for solving a GNLSE problem

@param stride number of steps before saving
@param total_strides total number of strides before ending the integration
@param saveDir directory to save the parameters of the system
@return Null
*/
void GNLSE_problem::solve(int stride, int total_strides, std::string saveDir)
{

	std::string saveDir_GNLSE = saveDir + std::string("//gnlse_field");
	CreateFolder(saveDir_GNLSE.c_str());

	if (nonlin.numbers.isempty() && !nonlin.vectors.isempty()) //problem with only vectors nonlinearities
	{
		//save mesh properties
		pmesh.saveparameters(saveDir);
		//save first state
		envelope.save_field(saveDir_GNLSE);
		//save nonlinearities
		nonlin.save_nonlinearities(saveDir);


		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_vectors(stride);
			
			//save field at each stride
			envelope.save_field(saveDir_GNLSE);
		}
	}

	else if (nonlin.vectors.isempty() && !nonlin.numbers.isempty()) //problem with only numbers nonlinearities
	{
		//save mesh properties
		pmesh.saveparameters(saveDir);
		//save first state
		envelope.save_field(saveDir_GNLSE);
		//save nonlinearities
		nonlin.save_nonlinearities(saveDir);

		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_numbers(stride);

			//save field at each stride
			envelope.save_field(saveDir_GNLSE);
		}

	}
	
	else if (!nonlin.vectors.isempty() && !nonlin.numbers.isempty()) //problem with both nonlinearities
	{
		//save mesh properties
		pmesh.saveparameters(saveDir);
		//save first state
		envelope.save_field(saveDir_GNLSE);
		//save nonlinearities
		nonlin.save_nonlinearities(saveDir);

		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_vectors_and_numbers(stride);

			//save field at each stride
			envelope.save_field(saveDir_GNLSE);
		}
	}

	else
	{
		std::cout << "\n\nThere must be at least one nonlinearity for the problem to evolve correctly!\n";
		std::cout << "Please, review your code!\n";

	}
	


}

/*
method for solving a GNLSE problem

@param stride number of steps before saving
@param total_strides total number of strides before ending the integration
@param saveDir directory to save the parameters of the system
@param window for plotting the abs(profile) at the end of each stride
@return Null
*/
void GNLSE_problem::solve(int stride, int total_strides, af::Window& win)
{


	if (nonlin.numbers.isempty() && !nonlin.vectors.isempty()) //problem with only vectors nonlinearities
	{
		//plot first state
		envelope.plot(win);

		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_vectors(stride);

			//plot envelope
			envelope.plot(win);
		}
	}

	else if (nonlin.vectors.isempty() && !nonlin.numbers.isempty()) //problem with only numbers nonlinearities
	{
		//plot first state
		envelope.plot(win);

		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_numbers(stride);

			//plot envelope
			envelope.plot(win);
		}

	}

	else if (!nonlin.vectors.isempty() && !nonlin.numbers.isempty()) //problem with both nonlinearities
	{
		//plot first state
		envelope.plot(win);

		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";
			//integration step for one stride
			push_vectors_and_numbers(stride);

			//plot envelope
			envelope.plot(win);
		}

	}

	else
	{
		std::cout << "\n\nThere must be at least one nonlinearity for the problem to evolve correctly!\n";
		std::cout << "Please, review your code!";

	}

}

/*
method for solving a GNLSE problem

@param stride number of steps before saving
@param total_strides total number of strides before ending the integration
@param saveDir directory to save the parameters of the system
@param window for plotting the abs(profile) at the end of each stride
@return Null
*/
void GNLSE_problem::solve(int stride, int total_strides, std::string saveDir, af::Window& win)
{


	if (nonlin.numbers.isempty() && !nonlin.vectors.isempty()) //problem with only vectors nonlinearities
	{
		std::string saveDir_GNLSE = saveDir + std::string("//gnlse_field");
		CreateFolder(saveDir_GNLSE.c_str());

		//save mesh properties
		pmesh.saveparameters(saveDir);
		//save first state
		envelope.save_field(saveDir_GNLSE);
		//save nonlinearities
		nonlin.save_nonlinearities(saveDir);


		//plot first state
		envelope.plot(win);

		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_vectors(stride);

			//save field at each stride
			envelope.save_field(saveDir_GNLSE);

			//plot envelope
			envelope.plot(win);
		}
	}

	else if (nonlin.vectors.isempty() && !nonlin.numbers.isempty() ) //problem with only numbers nonlinearities
	{
		std::string saveDir_GNLSE = saveDir + std::string("//gnlse_field");
		CreateFolder(saveDir_GNLSE.c_str());

		//save mesh properties
		pmesh.saveparameters(saveDir);
		//save first state
		envelope.save_field(saveDir_GNLSE);
		//save nonlinearities
		nonlin.save_nonlinearities(saveDir);


		//plot first state
		envelope.plot(win);

		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_numbers(stride);

			//save field at each stride
			envelope.save_field(saveDir_GNLSE);

			//plot envelope
			envelope.plot(win);
		}

	}

	else if(!nonlin.vectors.isempty() && !nonlin.numbers.isempty()) //problem with both nonlinearities
	{
		std::string saveDir_GNLSE = saveDir + std::string("//gnlse_field");
		CreateFolder(saveDir_GNLSE.c_str());

		//save mesh properties
		pmesh.saveparameters(saveDir);
		//save first state
		envelope.save_field(saveDir_GNLSE);
		//save nonlinearities
		nonlin.save_nonlinearities(saveDir);


		//plot first state
		envelope.plot(win);

		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";
			//integration step for one stride
			push_vectors_and_numbers(stride);

			//save field at each stride
			envelope.save_field(saveDir_GNLSE);

			//plot envelope
			envelope.plot(win);
		}

	}

	else
	{
		std::cout << "\n\nThere must be at least one nonlinearity for the problem to evolve correctly!\n";
		std::cout << "Please, review your code!";

	}

}

/*
method for solving a GNLSE problem - only for benchmark purposes - no data saving involved

@param stride number of steps before saving
@param total_strides total number of strides before ending the integration
@param saveDir directory to save the parameters of the system
@return Null
*/
void GNLSE_problem::benchmark_solve(int stride, int total_strides, std::string saveDir)
{

	//save mesh properties
	//pmesh.saveparameters(saveDir);
	//save first state
	//envelope.save_field(saveDir);
	//save nonlinearities
	//nonlin.save_nonlinearities(saveDir);


	if (nonlin.numbers.isempty() && !nonlin.vectors.isempty()) //problem with only vectors nonlinearities
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_vectors(stride);

		}
	}

	else if (nonlin.vectors.isempty() && !nonlin.numbers.isempty()) //problem with only numbers nonlinearities
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_numbers(stride);

		}

	}

	else if (!nonlin.vectors.isempty() && !nonlin.numbers.isempty()) //problem with both nonlinearities
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_vectors_and_numbers(stride);

		}

	}

	else
	{
		std::cout << "\n\nThere must be at least one nonlinearity for the problem to evolve correctly!\n";
		std::cout << "Please, review your code!";

	}


}