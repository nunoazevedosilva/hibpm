#pragma once

/************************************************
 *  HiBPM: a numerical module to solve the Generalized
 Nonlinear Schrödinger Poisson equation system(GNLSP).
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/

 /*
 definitions.h:
 Contains the definitions related to the precision, constants and methods
to be used
 */



#include <arrayfire.h>
#include <vector>
#include <numeric>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <functional>
#include <fstream>
#include <sstream>
#include <tuple>
#include <ctime>
#include <math.h>
#include <complex>
#include <cstdio>
#include <cstdlib>
#include <vector>


#define PI 3.14159265358979323846

//Double precision
#define Precision2 c64
#define Precision3 f64
typedef double Precision;
typedef af::af_cdouble cPrecision;

//Single precision - uncomment if needed
//#define Precision2 c32
//#define Precision3 f32
//typedef float Precision;
//typedef af::af_cfloat cPrecision;
