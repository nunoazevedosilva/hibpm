#pragma once

/************************************************
 *  HiBPM: a numerical module to solve the Generalized
 Nonlinear Schrödinger Poisson equation system(GNLSP).
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/

 /*
sample_GNLSP_problem.h:
functions to create a sample problem of the GNLSP type.
 */

#include "core.h"

af::array NLC_u_field_v(mesh pmesh, Precision x0, Precision q, Precision nu, Precision velocity, Precision phase = 0)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);
	new_vector = 3.0*q*af::pow((1.0 / af::cosh(sqrt(q / (2 * nu))*(pmesh.x() - x0))), 2.0)*af::exp(cPrecision(0, 1)*q*pmesh.t() / nu);
	new_vector *= 1.0 / (2.0*sqrt(2.0*nu))*af::exp(cPrecision(0, 1)*(velocity*pmesh.x() + phase));
	return new_vector;
}

af::array Gaussian_Beam(mesh pmesh, Precision P, Precision w0, Precision x0, Precision y0, Precision n, int l)
{

	Precision E0 = sqrt(2.0*P / (PI*w0*w0));


	af::array gaussian_beam = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);
	af::array theta = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	theta = af::atan2(pmesh.x() - x0, pmesh.y() - y0);


	gaussian_beam = E0*af::exp(-((pmesh.x() - x0)*(pmesh.x() - x0) + (pmesh.y() - y0)*(pmesh.y() - y0)) / (w0*w0));
	gaussian_beam *= af::exp(cPrecision(0, 1)*l*theta);
	gaussian_beam *= af::pow(sqrt(2)*af::sqrt((pmesh.x() - x0)*(pmesh.x() - x0) + (pmesh.y() - y0)*(pmesh.y() - y0)) / w0, l);


	return gaussian_beam;
}

void example_1d()
{
	//Simulation parameters
	int Dims = 1;
	int Nx = 1024;
	int Ny = 1;
	int Nt = 1;
	int Nz = 1;

	double dx = 130.0 / Nx;
	double dy = 1.0;
	double dt = 1.0;

	//integration parameters
	double dz = 0.001;


	//Problem parameters
	Precision LFactor = 0.5;                    //number factor in laplacian
	Precision NonLocalNonlinearity = -2.0;        //Intensity of the theta field nonlinearity
	Precision nu = 100.0;                        //Measure the nonlocal response
	Precision q = 1.0;                            //Rubbing angle or pre-tilting electric field
	Precision qStrength = -2.0;                    //Rubbing angle strengh
	Precision FieldSourceStrength = -2.0;        //Field strength


	mesh MyMesh = mesh(Dims, Nx, Ny, Nt,Nz, dx, dy, dt, dz);

	gnlse_field Envelope = gnlse_field(MyMesh,0);
	nonlocal_field ThetaField = nonlocal_field(MyMesh);

	Precision x0 = Nx*dx / 2.0;

	//Envelope.add_field(NLC_u_field_v(MyMesh, x0 - x0 / 2.0, q, nu, 1.3));
	//Envelope.add_field(NLC_u_field_v(MyMesh, x0 + x0 / 2.0, q, nu, -1.3));

	Envelope.add_field(sech_1d(MyMesh, cPrecision(0.2), cPrecision(0.5, 0), Nx*dx/4, 0));
	Envelope.add_field(sech_1d(MyMesh, cPrecision(0.1), cPrecision(-0.5, 0), 3* Nx*dx / 4, 0));


	nonlinear_terms_number NonLinNumbers = nonlinear_terms_number(MyMesh);
	//NonLinNumbers.add_nonlinear_term_by_values(cPrecision(1, 0), 2);
	nonlinear_terms_vector NonLinVectors = nonlinear_terms_vector(MyMesh);
	nonlinear_terms NL = nonlinear_terms(MyMesh, NonLinNumbers, NonLinVectors);


	GNLSE_problem GNLSE = GNLSE_problem(MyMesh, Envelope, NL, LFactor);
	Poisson_problem POISSON = Poisson_problem(MyMesh, ThetaField, Envelope.profile, nu, -q*qStrength, FieldSourceStrength);
	GNLSP_problem SN = GNLSP_problem(MyMesh, GNLSE, POISSON, -20*NonLocalNonlinearity);
	

	static int Width = 300, Height = 300;
	af::Window window1(Width, Height, "GNLSE Field");
	af::Window window2(Width, Height, "Poisson Field");

	//NematicCrystlasStationary1D.Solve(100, 500, window1);

	std::string save_path = "sim1";
	CreateFolder(save_path.c_str());

	SN.solve(100, 500, save_path,window1,window2);

};

void example_2d()
{
	//Simulation parameters
	int Dims = 2;
	int Nx = 400;
	int Ny = 400;
	int Nt = 1;
	int Nz = 1;

	double dx = 0.2;
	double dy = 0.2;
	double dt = 1.0;

	//integration parameters
	double dz = 0.01;


	//Problem parameters
	Precision LFactor = 0.5;                    //number factor in laplacian
	Precision NonLocalNonlinearity = -2.0;        //Intensity of the theta field nonlinearity
	Precision nu = 100.0;                        //Measure the nonlocal response
	Precision q = 1.0;                            //Rubbing angle or pre-tilting electric field
	Precision qStrength = -2.0;                    //Rubbing angle strengh
	Precision FieldSourceStrength = -2.0;        //Field strength


	mesh MyMesh = mesh(Dims, Nx, Ny, Nt, Nz, dx, dy, dt, dz);

	gnlse_field Envelope = gnlse_field(MyMesh, 0);
	nonlocal_field ThetaField = nonlocal_field(MyMesh);

	Precision x0 = Nx*dx / 2.0;
	Precision impact = 10;

	//Envelope.add_field(NLC_u_field_v(MyMesh, x0 - x0 / 2.0, q, nu, 1.3));
	//Envelope.add_field(NLC_u_field_v(MyMesh, x0 + x0 / 2.0, q, nu, -1.3));

	//Envelope.add_field(sech_1d(MyMesh, cPrecision(0.2), cPrecision(0.5, 0), Nx*dx / 4, 0));
	//Envelope.add_field(sech_1d(MyMesh, cPrecision(0.1), cPrecision(-0.5, 0), 3 * Nx*dx / 4, 0));
	//Envelope.add_field(gaussian_2d(MyMesh, cPrecision(0.2), cPrecision(10.0, 0), Nx*dx / 3, Ny*dy / 2 + impact,0.5,0));
	Envelope.add_field(gaussian_2d(MyMesh, cPrecision(0.2), cPrecision(10.0, 0), Nx*dx / 2, Ny*dy / 2, 0.1, 0));

	nonlinear_terms_number NonLinNumbers = nonlinear_terms_number(MyMesh);
	//NonLinNumbers.add_nonlinear_term_by_values(cPrecision(1, 0), 2);
	nonlinear_terms_vector NonLinVectors = nonlinear_terms_vector(MyMesh);
	nonlinear_terms NL = nonlinear_terms(MyMesh, NonLinNumbers, NonLinVectors);


	GNLSE_problem GNLSE = GNLSE_problem(MyMesh, Envelope, NL, LFactor);
	Poisson_problem POISSON = Poisson_problem(MyMesh, ThetaField, Envelope.profile, nu, -q*qStrength, FieldSourceStrength);
	GNLSP_problem SN = GNLSP_problem(MyMesh, GNLSE, POISSON, -2 * NonLocalNonlinearity);


	static int Width = 300, Height = 300;
	af::Window window1(Width, Height, "GNLSE Field");
	af::Window window2(Width, Height, "Poisson Field");

	//NematicCrystlasStationary1D.Solve(100, 500, window1);

	std::string save_path = "sim2";
	CreateFolder(save_path.c_str());

	SN.solve(200, 500, save_path, window1, window2);

};

void example_vortex_2d()
{
	//Simulation parameters
	int Dims = 2;
	int Nx = 400;
	int Ny = 400;
	int Nt = 1;
	int Nz = 1;

	double dx = 0.5;
	double dy = 0.5;
	double dt = 1.0;

	//integration parameters
	double dz = 0.01;


	//Problem parameters
	Precision LFactor = 0.5;                    //number factor in laplacian
	Precision NonLocalNonlinearity = -2.0;        //Intensity of the theta field nonlinearity
	Precision nu = 100.0;                        //Measure the nonlocal response
	Precision q = 1.0;                            //Rubbing angle or pre-tilting electric field
	Precision qStrength = -2.0;                    //Rubbing angle strengh
	Precision FieldSourceStrength = -2.0;        //Field strength


	mesh MyMesh = mesh(Dims, Nx, Ny, Nt, Nz, dx, dy, dt, dz);

	gnlse_field Envelope = gnlse_field(MyMesh, 0);
	nonlocal_field ThetaField = nonlocal_field(MyMesh);

	Precision x0 = Nx*dx / 2.0;
	Precision impact = 10;

	//Envelope.add_field(NLC_u_field_v(MyMesh, x0 - x0 / 2.0, q, nu, 1.3));
	//Envelope.add_field(NLC_u_field_v(MyMesh, x0 + x0 / 2.0, q, nu, -1.3));

	//Envelope.add_field(sech_1d(MyMesh, cPrecision(0.2), cPrecision(0.5, 0), Nx*dx / 4, 0));
	//Envelope.add_field(sech_1d(MyMesh, cPrecision(0.1), cPrecision(-0.5, 0), 3 * Nx*dx / 4, 0));
	Envelope.add_field(perfect_vortex_2d(MyMesh, cPrecision(0.4), cPrecision(7.0, 0), Nx*dx / 2, Ny*dy / 2 , 20.0, 3));
	nonlinear_terms_number NonLinNumbers = nonlinear_terms_number(MyMesh);
	//NonLinNumbers.add_nonlinear_term_by_values(cPrecision(1, 0), 2);
	nonlinear_terms_vector NonLinVectors = nonlinear_terms_vector(MyMesh);
	nonlinear_terms NL = nonlinear_terms(MyMesh, NonLinNumbers, NonLinVectors);


	GNLSE_problem GNLSE = GNLSE_problem(MyMesh, Envelope, NL, LFactor);
	Poisson_problem POISSON = Poisson_problem(MyMesh, ThetaField, Envelope.profile, nu, -q*qStrength, FieldSourceStrength);
	GNLSP_problem SN = GNLSP_problem(MyMesh, GNLSE, POISSON, -2 * NonLocalNonlinearity);


	static int Width = 300, Height = 300;
	af::Window window1(Width, Height, "GNLSE Field");
	af::Window window2(Width, Height, "Poisson Field");

	//NematicCrystlasStationary1D.Solve(100, 500, window1);

	std::string save_path = "sim2";
	CreateFolder(save_path.c_str());

	SN.solve(200, 500, save_path, window1, window2);

};


int example_scat_2d(std::string saveDir, int n)
{
	CreateFolder(saveDir.c_str());

	// Select a device and display arrayfire info
	int device = 0;
	af::setDevice(device);
	af::info();

	//getchar();

	printf("Creating a mesh for our problem \n");

	//define a new mesh for the problem
	int dims = 2;
	int Nx = 512;
	int Ny = 256;
	int Nt = 1;


	int Nz = 1;
	double dz = 0.5;
	int total_steps = 20;
	int stride = 1200;


	double dx = 2.0;
	double dy = 2.0;
	double dt = 1.;

	mesh mymesh = mesh(dims, Nx, Ny, Nt, Nz, dx, dy, dt, dz);

	printf("---Mesh created--- \n");


	//number nonlinearities
	nonlinear_terms_number NL_number = nonlinear_terms_number(mymesh);
	NL_number.add_nonlinear_term_by_values(cPrecision(-0.003), Precision(2));

	//vector nonlinearities
	nonlinear_terms_vector NL_vector = nonlinear_terms_vector(mymesh);
	NL_vector.add_nonlinear_term_by_function(Precision(0), gaussian_2d(mymesh, cPrecision(-1.0), cPrecision(5, 0), Precision(Nx*dx / 2.), Precision(Ny*dy / 2.)));




	//create the nonlinearities
	nonlinear_terms NL = nonlinear_terms(mymesh, NL_number, NL_vector);

	printf("---Nonlinearities created--- \n");

	//initialize a scalar field

	gnlse_field envelope = gnlse_field(mymesh, 0);
	envelope.add_field(planewave_2d(mymesh, 1.0, 2 * PI * n / (Nx*dx), 0));
	envelope.add_field(noise_2d(mymesh, cPrecision(0 * 0.1, 0)));

	printf("---scalar field created--- \n");

	//creating the GNLSE problem
	GNLSE_problem problem = GNLSE_problem(mymesh, envelope, NL, 0.5);

	//window creation
	const static int width = 512, height = 512;
	af::Window window3(width, height, "2D plot example title");


	af::sync();
	printf("---start--- \n");

	//clock initial
	auto start = std::chrono::high_resolution_clock::now();

	//integration routine
	problem.solve(stride, total_steps, saveDir, window3);

	//clock final
	auto end = std::chrono::high_resolution_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "Total elapsed time " << "\t" << elapsed.count() << std::endl;

	printf("-----------Finalized, press enter to exit...\n");
	getchar();

	return 0;
}
