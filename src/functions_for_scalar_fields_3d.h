#pragma once

#include "definitions.h"
#include "mesh.h"



/**

functions_for_scalar_fields_3d.h
Purpose: Various functions definitions for generating different scalar field profiles

@author Nuno Azevedo Silva
@version 1.0

*/


/*
gaussian_3d creates a vector with a 3d gaussian state

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param w is the width
@param x0 is the center in x-direction
@param y0 is the center in y-direction
@param t0 is the center in t-direction
@param vx is the velocity in x - direction
@param vy is the velocity in y - direction
@param vt is the velocity in t - direction
@return array
*/
af::array gaussian_3d(mesh pmesh, cPrecision A, cPrecision w, Precision x0, Precision y0, Precision t0, Precision vx, Precision vy, Precision vt)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::exp(-((pmesh.x() - x0)*(pmesh.x() - x0) + (pmesh.y() - y0)*(pmesh.y() - y0) + (pmesh.t() - t0)*(pmesh.t() - t0)) / (w*w))*af::exp(cPrecision(0, 1)*(vx*(pmesh.x() - x0) + vy*(pmesh.y() - y0) + vt*(pmesh.t() - t0)));

	return new_vector;
}