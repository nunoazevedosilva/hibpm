#pragma once

/************************************************
 *  HiBPM: a numerical module to solve the Generalized 
 Nonlinear Schrödinger Poisson equation system(GNLSP). 
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/

 /* 
 core.h: 
 Defines the libraries to be loaded and used in the project
 */

#include <arrayfire.h>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include "Misc.h"
#include "mesh.h"
#include "gnlse_field.h"
#include "GNLSE_problem.h"
#include "GNLSP_problem.h"
#include "nonlinear_Terms.h"
#include "nonlinear_terms_number.h"
#include "nonlinear_terms_vector.h"
#include "functions_for_scalar_fields_1d.h"
#include "functions_for_scalar_fields_2d.h"
#include "functions_for_scalar_fields_3d.h"