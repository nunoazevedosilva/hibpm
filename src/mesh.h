#pragma once


/************************************************
 *  HiBPM: a numerical module to solve the Generalized
 Nonlinear Schrödinger Poisson equation system(GNLSP).
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/

 /*
mesh.h: The class mesh defines an instance that corresponds to the (3+1)-d simulation
mesh. This instance is shared by all the modules and contains auxiliary functions
 */

#include "definitions.h"
#include "Misc.h"



//class mesh - object mesh to contain the mesh of the problem
class mesh{

public:

	int dims;	//number of dimensions

	int Nx;		//number of points in the x dimension
	int Ny;		//number of points in the y dimension
	int Nz;		//number of points in the z dimension
	int Nt;		//number of points in the t dimension

	Precision Lx;	//length of the simulation box in the x dimension
	Precision Ly;	//length of the simulation box in the y dimension
	Precision Lz;	//length of the simulation box in the z dimension
	Precision Lt;	//length of the simulation box in the t dimension
	
	Precision dx;	//spacing in x dimension
	Precision dy;	//spacing in y dimension
	Precision dt;	//spacing in t dimension

	Precision dz;	//spacing in the z dimension - integration direction
	
	Precision current_z; //current z coordinate during an integration
	int current_step_number; //current step_number in the integration cycle


	mesh(int dims, int Nx, int Ny, int Nt, int Nz, Precision dx, Precision dy, Precision dt, Precision dz);

	af::array x();

	af::array y();

	af::array z();

	af::array t();

	void saveparameters(std::string saveDir);

	void print();

	void print_xyzt();
};


/*
Mesh class constructor

@param dim Number of dimensions in our system
@param Nx Number of points in X direction
@param Ny Number of points in Y direction
@param Nt Number of points in Z direction
@param Nz Number of points in Z direction
@param dx Point spacing in X direction
@param dy Point spacing in Y direction
@param dt Temporal integration step
@param dz Point spacing in Z direction
@return Null
*/
mesh::mesh(int dims, int Nx, int Ny, int Nt, int Nz, Precision dx, Precision dy, Precision dt, Precision dz) : dims(dims), Nx(Nx), Ny(Ny), Nz(Nz), Nt(Nt),
dx(dx), dy(dy), dz(dz), dt(dt),
Lx(Nx*dx), Ly(Ny*dy), Lz(Nz*dz), Lt(Nt*dt), current_z(Precision(0)), current_step_number(Precision(0)) {}

//return a x - multidimensional array
af::array mesh::x() {

	return this->dx*af::range(this->Nx, this->Ny, this->Nt, 1, 0, Precision3);

}

//create a y - multidimensional array
af::array mesh::y() {

	return this->dy*af::range(this->Nx, this->Ny, this->Nt, 1, 1, Precision3);
}

//create a z - multidimensional array
af::array mesh::z() {
	
	return this->dz*af::range(this->Nx, this->Ny, this->Nt, this->Nz, 3, Precision3);

}

//create a t - multidimensional array
af::array mesh::t() {

	return this->dt*af::range(this->Nx, this->Ny, this->Nt, 1, 2, Precision3);

}

//save parameters of the mesh to file
void mesh::saveparameters(std::string saveDir) {
	std::ofstream param(saveDir + std::string("/parameters.dat"));
	
	//print dimensions
	param << "Dimension\n";
	param << dims << "\n";
	
	//print dh
	param << "dh\n";
	param << dx << " \n";
	param << dy << " \n";
	param << dt << " \n";
	param << dz << " \n";
	
	//print N points
	param << "Npoints\n";
	param << Nx << " \n";
	param << Ny << " \n";
	param << Nt << " \n";
	param << Nz << " \n";
	
	//print limits
	param << "Limits\n";
	param << Lx << " \n";
	param << Ly << " \n";
	param << Lt << " \n";
	param << Lz << " \n";
	
	param.close();

}

//print mesh parameters
void mesh::print() {

	//print dimensions
	std::cout << "Dimension\n";
	std::cout << dims << "\n";

	//print dh and N points
	std::cout << "dx\t Nx\n";
	std::cout << dx << "\t " << Nx << "\n" << std::endl;
	
	std::cout << "dy\t Ny\n";
	std::cout << dy<< "\t " << Ny << "\n" << std::endl;

	std::cout << "dt\t Nt\n";
	std::cout << dt << "\t " << Nt << "\n" << std::endl;

	std::cout << "dz\t Nz\n";
	std::cout << dz << "\t " << Nz << "\n" << std::endl;


	//print limits
	std::cout << "Limits\n";
	std::cout << "Lx" << "\t " << Lx << " \n";
	std::cout << "Ly" << "\t " << Ly << " \n";
	std::cout << "Lt" << "\t " << Lt << " \n";
	std::cout << "Lz" << "\t " << Lz << " \n";


}


//print all mesh parameters, debug purposes
void mesh::print_xyzt()
{
	std::cout << "----- X Mesh Vector -----" << std::endl;
	af_print(x());
	std::cout << "----- Y Mesh Vector -----" << std::endl;
	af_print(y());
	std::cout << "----- Z Mesh Vector -----" << std::endl;
	af_print(z());
	std::cout << "----- T Mesh Vector -----" << std::endl;
	af_print(t());
}