#pragma once

#include "definitions.h"
#include "mesh.h"



/**

functions_for_scalar_fields_1d.h
Purpose: Various functions definitions for generating different scalar field profiles

@author Nuno Azevedo Silva
@version 1.0

*/

/*
gaussian_1d creates a vector with a 1d gaussian state

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param w is the width
@param x0 is the center x
@return array
*/
af::array gaussian_1d(mesh pmesh, cPrecision A, cPrecision w, Precision x0)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::exp(-((pmesh.x() - x0)*(pmesh.x() - x0)) / (w*w));

	return new_vector;
}

/*
gaussian_1d creates a vector with a 1d gaussian state with a velocity

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param w is the width
@param x0 is the center x
@return array
*/
af::array gaussian_1d(mesh pmesh, cPrecision A, cPrecision w, Precision x0, Precision k)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::exp(-((pmesh.x() - x0)*(pmesh.x() - x0)) / (w*w))*af::exp(cPrecision(0,1)*k*(pmesh.x() - x0));

	return new_vector;
}

/*
dissipative_soliton creates a vector with a 1d gaussian state with a velocity

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param w is the width
@param x0 is the center x
@return array
*/
af::array dissipative_soliton(mesh pmesh, cPrecision A, cPrecision B, Precision x0, Precision k, Precision d)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::exp(cPrecision(1,d)*af::log(1.0 /af::cosh(B*(pmesh.x() - x0))))*af::exp(cPrecision(0, 1)*k*(pmesh.x() - x0));

	return new_vector;
}


/*
exp_1d creates a vector with a 1d exponential state

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param x0 is the center in x-direction
@return array
*/
af::array exp_1d(mesh pmesh, cPrecision A, Precision x0)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::exp(pmesh.x() - x0);

	return new_vector;
}


/*
wave_1d creates a vector with a 1d co-sine wave state

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param f is the frequency
@return array
*/
af::array wave_1d(mesh pmesh, cPrecision A, cPrecision f)
{

	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += A * af::cos(f*pmesh.x());

	return new_vector;

};

/*
sech_1d creates a vector with a 1d sech envelope state

@param pmesh is the mesh where we construct our scalar field vector
@param v is the amplitude
@param us is the k, velocity in x direction
@param x0 is the center x
@param phase is the initial phase for the state
@return array
*/
af::array sech_1d(mesh pmesh, cPrecision v, cPrecision us, Precision x0, Precision phase)
{

	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	new_vector += 2.0*v*(1.0 / af::cosh(2.0*v*(pmesh.x() - x0)))*af::exp(cPrecision(0, 2)*us*(pmesh.x() - x0) + cPrecision(0, 1)*phase);

	return new_vector;

};


/*
dark_1d_chain creates a vector with a 1d darksoliton chain envelope state

*/
af::array dark_1d_train(mesh pmesh, Precision A, Precision B, int n)
{

	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);


	Precision dx = pmesh.Nx*pmesh.dx / n ;
	Precision xi_p = 0;
	Precision xi = pmesh.Nx*pmesh.dx / n;
	new_vector += sqrt(A)*B*(pmesh.x()<xi)*af::tanh(sqrt(A)*B*(pmesh.x() - (xi_p+0.5*dx+6)));
	
	for (int i = 1; i < n-1; i++)
	{
		xi_p += dx;
		xi += dx;
		new_vector += sqrt(A)*B*(pmesh.x() < xi)*(pmesh.x() >= xi_p)*pow(-1,i)*af::tanh(sqrt(A)*B*(pmesh.x() - (xi_p + 0.5*dx)));
	}

	xi_p += dx;
	xi += dx;
	new_vector += sqrt(A)*B*(pmesh.x() >= xi_p)*pow(-1, n-1)*af::tanh(sqrt(A)*B*(pmesh.x() - (xi_p + 0.5*dx)));

	return new_vector;

};
/*
square_1d creates a vector with a 1d square wave envelope state

@param pmesh is the mesh where we construct our scalar field vector
@param v is the amplitude
@param L is the width of the pulse
@param x0 is the center x
@param phase is the initial phase for the state
@return array
*/
af::array square_1d(mesh pmesh, cPrecision v, Precision L, Precision x0, Precision phase)
{

	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

	af::array m = ((pmesh.x() - x0) < (x0 + L/2) )&& ((pmesh.x() - x0) > (x0 - L / 2));

	new_vector += v*m;

	return new_vector;

};

/*
supergaussian_1d creates a vector with a 1d supergaussian state

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude
@param a is the width parameter
@param m is the power
@param zxs is the k, velocity in x direction
@param x0 is the center x
@param phase is the initial phase for the state
@param xs is a scale in x direction
@return array
*/
af::array supergaussian_1d(mesh pmesh, cPrecision A = 1, cPrecision a = 1, Precision m = 1, Precision zxs = 0, Precision x0 = 0, Precision phase = 0, Precision xs = 1 )
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);
	new_vector += A*af::exp( af::pow(((pmesh.x()*xs - x0)*(pmesh.x()*xs - x0)) / (a*a), m) / cPrecision(-2.) + cPrecision(0, 1)*zxs*(pmesh.x() - x0) + cPrecision(0, 1)*phase);
	return new_vector;
};


/*
pump_probe creates a vector for a pump-probe experiment

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude of the pump
@param B is the amplitude of the probe
@return array
*/
af::array pump_probe(mesh pmesh, cPrecision A, cPrecision B, cPrecision k)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);
	//new_vector += A + B* af::cos(pmesh.x()*k);
	new_vector += A + B* af::exp(cPrecision(0, 1.0)*pmesh.x()*k);
	return new_vector;
};

/*
rogue_start creates a vector for a rogue_wave experiment

@param pmesh is the mesh where we construct our scalar field vector
@param A is the amplitude of the pump
@param B is the amplitude of the probe
@return array
*/
af::array rogue_start(mesh pmesh, cPrecision A, cPrecision B, Precision w, Precision x0, Precision delta)
{
	af::array new_vector = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);
	//new_vector += A + B* af::cos(pmesh.x()*k);

	af::array rd = (af::randn(pmesh.Nx, pmesh.Ny, pmesh.Nt) - af::constant(1., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2) *0.5)* (B);
	
	delta = sqrt(-w*w*log(0.1));
	Precision A1 = exp((delta)*(delta) / (w*w));
	new_vector += A*A1*af::exp(-(pmesh.x() - x0)*(pmesh.x() - x0) / (w*w))*(pmesh.x() <= (x0 - delta));
	new_vector += A*A1*af::exp(-(pmesh.x() - x0)*(pmesh.x() - x0) / (w*w))*(pmesh.x() >= (x0 + delta));
	new_vector += A*A1*af::exp(-(pmesh.x()>0)*(delta)*(delta)/(w*w))*(pmesh.x() > (x0 - delta)&&pmesh.x() < (x0 + delta));

	new_vector += rd*(pmesh.x() > (x0 - delta) && pmesh.x() < (x0 + delta));// +B* af::exp(cPrecision(0, -1.0)*pmesh.x()*k);
	return new_vector;
};
