/************************************************
 *  HiBPM: a numerical module to solve the Generalized
 Nonlinear Schrödinger Poisson equation system(GNLSP).
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/

 /*
Source.cpp
 */


#include "core.h"
#include "sample_GNLSP_problem.h"

using namespace af;


int main(int argc, char *argv[])
{

	// Select backend

	std::cout << "Select Backend" << std::endl;
	std::cout << "1.CPU" << std::endl;
	std::cout << "2.OPENCL" << std::endl;
	std::cout << "3.CUDA" << std::endl;
	int backend;
	std::cout << "Select Backend (number): ";
	std::cin >> backend;
	std::cout << std::endl;
	if (backend == 1) { af_set_backend(AF_BACKEND_CPU); }
	else if (backend == 2) { af_set_backend(AF_BACKEND_OPENCL); }
	else if (backend == 3) { af_set_backend(AF_BACKEND_CUDA); }
	else { std::cout << "Error - invalid backend" << std::endl; }	

	// Select a device after displaying info
	af::info();

	//select the device
	int device;
	std::cout << "Select device number: ";
	std::cin >> device;
	std::cout<<std::endl;
	af::setDevice(device);

	//select Backend


	std::cout << "Running info";
	af::info();

	//running an example
	example_scat_2d("data_folder",10);

	return 0;
}

