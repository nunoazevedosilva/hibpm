#pragma once
#include "definitions.h"
#include "mesh.h"
#include "nonlinear_terms_number.h"
#include "nonlinear_terms_vector.h"


/**

nonlinear_terms.h
Purpose: Define a class for containing all the nonlinear and linear terms of both types 
defined in nonlinear_terms_vector.h and nonlinear_Terms_number.h, that enter in the SVEA approximation model.

@author Nuno Azevedo Silva
@version 1.0

*/

class nonlinear_terms
{

public:

	nonlinear_terms_number& numbers;			//number nonlinearities
	nonlinear_terms_vector& vectors;			//vector nonlinearities


	mesh& pmesh;							//mesh of our problem

	bool has_numbers;
	bool has_vectors;
	bool has_nonlocal;
	bool has_coupled;
	
	nonlinear_terms(mesh& pmesh, nonlinear_terms_number& numbers);
	nonlinear_terms(mesh& pmesh, nonlinear_terms_number& numbers, nonlinear_terms_vector& vectors);
	void save_nonlinearities(std::string saveDir);


};

/*
nonlinear_terms class constructor giving the terms

@param pmesh - mesh of our problem
@param numbers - number nonlinearities, previously defined by user
@param vectors - vector type nonlinearities, previously defined by user
@return Null
*/
nonlinear_terms::nonlinear_terms(mesh& pmesh, nonlinear_terms_number& numbers, nonlinear_terms_vector& vectors) :
	pmesh(pmesh), numbers(numbers), vectors(vectors) {}

//method for saving the nonlinearities
void nonlinear_terms::save_nonlinearities(std::string saveDir)
{
	numbers.save_nonlinearities(saveDir);
	vectors.save_nonlinearities(saveDir);
}

