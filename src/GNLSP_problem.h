#pragma once


#include "definitions.h"
#include "mesh.h"
#include "GNLSE_problem.h"
#include "Poisson_problem.h"

/************************************************
 *  HiBPM: a numerical module to solve the Generalized
 Nonlinear Schrödinger Poisson equation system(GNLSP).
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/

 /*
GNLSE_problem.h:
Given a GNLSE_problem and a Poisson_problem this class
creates an instance to solve the coupled GNLSP through the 
defined routines in the push and solve methods.
 */

class GNLSP_problem
{

public:

	GNLSE_problem& GNLSE;			//the GNLSE_problem to solve coupled to the SP
	Poisson_problem& POISSON;		//the SP to solve coupled to the GNLSE
	mesh& pmesh;					//our mesh for the problem
	
	cPrecision b;					//coupling constant

	af::array k2;

	GNLSP_problem(mesh& pmesh, GNLSE_problem& GNLSE, Poisson_problem& POISSON, cPrecision b);

	void nonlocal_step(Precision dz);

	void push(int stride);

	void solve(int stride, int total_strides, std::string saveDir);

	void solve(int stride, int total_strides, std::string saveDir, af::Window& win1, af::Window& win2);

	void benchmark_solve(int stride, int total_strides, std::string saveDir);

	void push_vectors_and_numbers(int stride);

	void push_vectors(int stride);

	void push_numbers(int stride);

	void plot(af::Window& win1, af::Window& win2);

};

/*
GNLSP_problem class constructor

@param pmesh is the mesh where we construct our scalar field vector
@param envelope is the profile, should be a previously created scalar field
@param l_factor is the number factor in the laplacian
@param nl_integrator is the type of integrator for the nonlinear step, "Euler"
@return Null
*/
GNLSP_problem::GNLSP_problem(mesh& pmesh, GNLSE_problem& GNLSE, Poisson_problem& POISSON, cPrecision b) : 
	pmesh(pmesh),GNLSE(GNLSE), POISSON(POISSON), b(b) 
{
	k2 = k2_nd(pmesh.dx, pmesh.dy, pmesh.dz, GNLSE.envelope.profile.dims(), Precision3);
	k2.eval();
}


//method for performing a nonlinear step with nonlocal field
void GNLSP_problem::nonlocal_step(Precision dz)
{
	GNLSE.envelope.profile *= af::exp(cPrecision(0, 1)*dz*b*POISSON.field_nonlocal.profile);
	GNLSE.envelope.profile.eval();
}

//method for a time integration stride for the usual Nonlinear Schrodinger equation - both vectors and numbers
void GNLSP_problem::push_vectors_and_numbers(int stride)
{
	Precision dz = pmesh.dz;

	//first a half linear step
	GNLSE.linear_step(dz/2);
	GNLSE.envelope.profile.eval();

	for (int step = 0; step < stride-1; step++)
		{
			//now perform each nonlinear step
			GNLSE.nonlinear_step_vector_euler(dz);//nonlinear step with vector nonlinearities
			GNLSE.nonlinear_step_number_euler(dz);//nonlinear step with number nonnlinearities
			POISSON.Poisson_solver();
			nonlocal_step(dz);
			//followed by a full dt linear step
			GNLSE.linear_step(dz);
			GNLSE.envelope.profile.eval();
		}
		
		//last nonlinear step for the stride
		GNLSE.nonlinear_step_vector_euler(dz);//nonlinear step with vector nonlinearities
		GNLSE.nonlinear_step_number_euler(dz);//nonlinear step with number nonnlinearities
		POISSON.Poisson_solver();
		nonlocal_step(dz);

		//a half linear step ends the stride
		GNLSE.linear_step(dz/2);
		GNLSE.envelope.profile.eval();
	

	//update step number and time coordinate
	pmesh.current_step_number += stride;
	pmesh.current_z += pmesh.dz * stride;
}

//method for a time integration stride for the usual Nonlinear Schrodinger equation - both vectors and numbers
void GNLSP_problem::push_vectors(int stride)
{
	Precision dz = pmesh.dz;

	//first a half linear step
	GNLSE.linear_step(dz / 2);


	for (int step = 0; step < stride - 1; step++)
	{
		//now perform each nonlinear step
		GNLSE.nonlinear_step_vector_euler(dz);//nonlinear step with vector nonlinearities
		POISSON.Poisson_solver();
		nonlocal_step(dz);
		//followed by a full dt linear step
		GNLSE.linear_step(dz);
	
	}

	//last nonlinear step for the stride
	GNLSE.nonlinear_step_vector_euler(dz);//nonlinear step with vector nonlinearities
	POISSON.Poisson_solver();
	nonlocal_step(dz);
	//a half linear step ends the stride
	GNLSE.linear_step(dz / 2);



	//update step number and time coordinate
	pmesh.current_step_number += stride;
	pmesh.current_z += pmesh.dz * stride;
}

//method for a time integration stride for the usual Nonlinear Schrodinger equation - both vectors and numbers
void GNLSP_problem::push_numbers(int stride)
{
	Precision dz = pmesh.dz;

	//first a half linear step
	GNLSE.linear_step(dz / 2);
	

	for (int step = 0; step < stride - 1; step++)
	{
		//now perform each nonlinear step
		GNLSE.nonlinear_step_number_euler(dz);//nonlinear step with number nonnlinearities
		POISSON.Poisson_solver();
		nonlocal_step(dz);
		//followed by a full dt linear step
		GNLSE.linear_step(dz);
		
	}

	//last nonlinear step for the stride
	GNLSE.nonlinear_step_number_euler(dz);//nonlinear step with number nonnlinearities
	POISSON.Poisson_solver();
	nonlocal_step(dz);
										  //a half linear step ends the stride
	GNLSE.linear_step(dz / 2);
	


	//update step number and time coordinate
	pmesh.current_step_number += stride;
	pmesh.current_z += pmesh.dz * stride;
}

void GNLSP_problem::plot(af::Window& win1, af::Window& win2)
{
	GNLSE.envelope.plot(win1);
	POISSON.field_nonlocal.plot(win2);

}

/*
method for solving a GNLSP problem

@param stride number of steps before saving
@param total_strides total number of strides before ending the integration
@param saveDir directory to save the parameters of the system
@return Null
*/
void GNLSP_problem::solve(int stride, int total_strides, std::string saveDir)
{

	//save mesh properties
	pmesh.saveparameters(saveDir);
	//save first state
	std::string saveDir_GNLSE = saveDir + std::string("//gnlse_field");
	CreateFolder(saveDir_GNLSE.c_str());
	
	//save nonlinearities
	std::string saveDir_POISSON = saveDir + std::string("//poisson_field");
	CreateFolder(saveDir_POISSON.c_str());
	GNLSE.nonlin.save_nonlinearities(saveDir);

	GNLSE.envelope.save_field(saveDir_GNLSE);
	POISSON.field_nonlocal.save_field(saveDir_POISSON);


	if (GNLSE.nonlin.numbers.isempty())//problem with only vectors nonlinearities
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_vectors(stride);
			
			//save field at each stride
			GNLSE.envelope.save_field(saveDir_GNLSE);
			POISSON.field_nonlocal.save_field(saveDir_POISSON);
		}
	}

	else if (GNLSE.nonlin.vectors.isempty())//problem with only numbers nonlinearities
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_numbers(stride);

			//save field at each stride
			GNLSE.envelope.save_field(saveDir_GNLSE);
			POISSON.field_nonlocal.save_field(saveDir_POISSON);
		}

	}
	
	else
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";
			//integration step for one stride
			push_vectors_and_numbers(stride);

			//save field at each stride
			GNLSE.envelope.save_field(saveDir_GNLSE);
			POISSON.field_nonlocal.save_field(saveDir_POISSON);
		}

	}


}

/*
method for solving a GNLSP problem

@param stride number of steps before saving
@param total_strides total number of strides before ending the integration
@param saveDir directory to save the parameters of the system
@return Null
*/
void GNLSP_problem::solve(int stride, int total_strides, std::string saveDir, af::Window& win1, af::Window& win2)
{

	//save mesh properties
	pmesh.saveparameters(saveDir);
	//save first state
	std::string saveDir_GNLSE = saveDir + std::string("//gnlse_field");
	CreateFolder(saveDir_GNLSE.c_str());

	//save nonlinearities
	std::string saveDir_POISSON = saveDir + std::string("//poisson_field");
	CreateFolder(saveDir_POISSON.c_str());
	GNLSE.nonlin.save_nonlinearities(saveDir);

	GNLSE.envelope.save_field(saveDir_GNLSE);
	POISSON.field_nonlocal.save_field(saveDir_POISSON);

	plot(win1, win2);


	if (GNLSE.nonlin.numbers.isempty())//problem with only vectors nonlinearities
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_vectors(stride);

			//save field at each stride
			GNLSE.envelope.save_field(saveDir_GNLSE);
			POISSON.field_nonlocal.save_field(saveDir_POISSON);

			plot(win1, win2);
		}
	}

	else if (GNLSE.nonlin.vectors.isempty())//problem with only numbers nonlinearities
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_numbers(stride);

			//save field at each stride
			GNLSE.envelope.save_field(saveDir_GNLSE);
			POISSON.field_nonlocal.save_field(saveDir_POISSON);

			plot(win1, win2);
		}

	}

	else
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";
			//integration step for one stride
			push_vectors_and_numbers(stride);

			//save field at each stride
			GNLSE.envelope.save_field(saveDir_GNLSE);
			POISSON.field_nonlocal.save_field(saveDir_POISSON);

			plot(win1, win2);
		}

	}


}


/*
method for solving a GNLSP problem - benchmarking purposes - no save

@param stride number of steps before saving
@param total_strides total number of strides before ending the integration
@param saveDir directory to save the parameters of the system
@return Null
*/
void GNLSP_problem::benchmark_solve(int stride, int total_strides, std::string saveDir)
{

	//save mesh properties
	//pmesh.saveparameters(saveDir);

	if (GNLSE.nonlin.numbers.isempty())//problem with only vectors nonlinearities
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_vectors(stride);
			//GNLSE.envelope.eval();
			//POISSON.field_nonlocal.eval();

		}
	}

	else if (GNLSE.nonlin.vectors.isempty())//problem with only numbers nonlinearities
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";

			//integration step for one stride
			push_numbers(stride);

			//GNLSE.envelope.eval();
			//POISSON.field_nonlocal.eval();

		}

	}

	else
	{
		for (int s = 0; s < total_strides; s++)
		{
			//print the number of the stride for indication purposes
			std::cout << "Stride " << std::to_string(s) << " of " << std::to_string(total_strides) << "\n";
			//integration step for one stride
			push_vectors_and_numbers(stride);

			//GNLSE.envelope.eval();
			//POISSON.field_nonlocal.eval();
		}

	}


}

