#pragma once

/************************************************
 *  HiBPM: a numerical module to solve the Generalized
 Nonlinear Schrödinger Poisson equation system(GNLSP).
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/

 /*
Misc.h:
contains definitions of functions important for different classes,
like the constructor for the k array, related with the spatial frequencies 
of the Fourier transform.
 */


#include "definitions.h"
#include <stdio.h>
#include <filesystem>

//function to make a copy of stdvector X1 to an arrayfire vector X2
void copy_std_to_af_1D(std::vector<Precision> X1, af::array X2) 
{
	for (int i = 0; i < X1.size(); i++)
	{
		X2(i) = X1[i];
	}
}

//function for creating the frequencies for an fft
af::array fftfreq(int n, double d, af::dtype type) {
	if (n % 2 == 0)
		return af::join(0, af::range(n / 2, 1, 1, 1, -1, type), -1.0*(af::flip(af::range(n / 2, 1, 1, 1, -1, type), 0) + 1.0)) / (n*d);
	else
		return af::join(0, af::range((n - 1) / 2 + 1, 1, 1, 1, -1, type), -1.0*(af::flip(af::range((n - 1) / 2, 1, 1, 1, -1, type), 0) + 1.0)) / (n*d);
}

//generate a kx for the multidimensional case
af::array kx_nd(af::array kx, af::dim4 dimens) {
	
	af::array kx_d = tile(moddims(kx, kx.elements(),1,1,1), 1, dimens[1], dimens[2]);

	return kx_d;
}

//generate a ky for the multidimensional case
af::array ky_nd(af::array ky, af::dim4 dimens) {

	af::array ky_d = tile(moddims(ky, 1, ky.elements(), 1, 1), dimens[0], 1, dimens[2]);

	return ky_d;
}

//generate a kt for the multidimensional case
af::array kt_nd(af::array kt, af::dim4 dimens) {

	af::array kt_d = tile(moddims(kt, 1, 1, kt.elements(), 1), dimens[0], dimens[1], 1);

	return kt_d;
}

//generate k^2 for the multidimensional case
af::array k2_nd(double dx, double dy, double dz, af::dim4 dimens, af::dtype type) 
{

	af::array kx = kx_nd(2.0*af::Pi*fftfreq(dimens[0], dx, type), dimens);
	af::array ky = ky_nd(2.0*af::Pi*fftfreq(dimens[1], dy, type), dimens);
	af::array kt = kt_nd(2.0*af::Pi*fftfreq(dimens[2], dz, type), dimens);

	af::array k2 = af::pow(kx, 2)+ af::pow(ky, 2)+af::pow(kt,2);

	return k2;

}

//generate k for the multidimensional case
af::array k_nd(double dx, double dy, double dz, af::dim4 dimens, af::dtype type)
{

	af::array kx = kx_nd(2.0*af::Pi*fftfreq(dimens[0], dx, type), dimens);
	af::array ky = ky_nd(2.0*af::Pi*fftfreq(dimens[1], dy, type), dimens);
	af::array kt = kt_nd(2.0*af::Pi*fftfreq(dimens[2], dz, type), dimens);

	af::array k = kx + ky + kt;

	return k;

}


//function to create a directory if inexistent
void CreateFolder(const char * path)
{
	std::filesystem::create_directories(path);
	/*
	if (!CreateDirectory(path, NULL))
	{
		return;
	}
	*/
}



